Rails.application.routes.draw do  
  devise_for :users
  resources :orders do
    resources :things
    resources :orders_users
    resources :settlements
  end
  root 'orders#index'
end
