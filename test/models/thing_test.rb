require 'test_helper'
require 'securerandom'

class ThingTest < ActiveSupport::TestCase
  test "Should not save when thing has not name" do
    @thing = Thing.new
    @thing.order = Order.new
    @thing.cost = 1
    assert_not @thing.save
  end

  test "Should not save when thing name has length 21" do
    @thing = Thing.new
    @thing.order = Order.new
    @thing.cost = 1
    @thing.name = SecureRandom.urlsafe_base64(16)
    assert_not @thing.save
  end

  test "Should not save when thing has not order" do
    @thing = Thing.new
    @thing.cost = 1
    @thing.name = SecureRandom.urlsafe_base64(10)
    assert_not @thing.save
  end

  test "Should not save when thing has not cost" do
    @thing = Thing.new
    @thing.order = Order.new
    @thing.name = SecureRandom.urlsafe_base64(10)
    assert_not @thing.save
  end

  test "Should save when thing has cost and name with good length and order" do
    @thing = Thing.new
    @thing.order = Order.new
    @thing.cost = 1
    @thing.name = SecureRandom.urlsafe_base64(10)
    puts @thing.cost
    puts @thing.name
    assert @thing.save
  end
end
