require 'test_helper'
require 'securerandom'

class OrderTest < ActiveSupport::TestCase
  test "Should not save when order has not name" do
    @order = Order.new
    assert_not @order.save
  end
  
  test "Should not save when order has length 2" do
    @order = Order.new
    @order.name = SecureRandom.urlsafe_base64(1)
    assert_not @order.save
  end

  test "Should not save when order has length 21" do
    @order = Order.new
    @order.name = SecureRandom.urlsafe_base64(16)
    assert_not @order.save
  end

 test "Should not save when order has status different that enum" do
    @order = Order.new
    assert_raises(ArgumentError){
      @order.status = SecureRandom.urlsafe_base64(10)
    }
  end

  test "Should save when order has status as that enum and name with good length" do
    @order = Order.new
    @order.status = 'open'
    @order.name = SecureRandom.urlsafe_base64(10)
    assert @order.save
  end
end
