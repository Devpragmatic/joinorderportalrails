class Thing < ApplicationRecord
  validates :name, length: { in: 1..20 }
  validates :cost, presence: true
  validates :user, presence: true
  validates :order, presence: true
  belongs_to :order
  belongs_to :user

  def is_owner?(user)
    self.user.id == user.id
  end

  def can_delete?(user)
    self.is_owner?(user) || self.order.is_owner?(user)
  end
end
