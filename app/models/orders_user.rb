class OrdersUser < User
    has_and_belongs_to_many :orders, join_table: :orders_orders_users
end
