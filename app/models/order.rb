class Order < ApplicationRecord
  validates :name, length: { in: 3..20 }
  validates :owner, presence: true
  enum status: [ :open, :close, :finish ]
  has_many :things, dependent: :destroy
  has_many :settlements, dependent: :destroy
  has_and_belongs_to_many :orders_users, join_table: :orders_orders_users
  belongs_to :owner, :class_name => 'User'
  belongs_to :paymaster, :class_name => 'User', optional: true

  # @return [label, value]
  def self.translated_status_keys_for_select
    statuses.map do |status, _|
      [translated_status_key(status), status]
    end
  end

  def self.translated_status_key(key)
    I18n.t("activerecord.enums.#{model_name.i18n_key}.status.#{key}")
  end

  def create_thing(thing_params)
    thing = self.things.create(thing_params)
    if(!thing.errors.any?)
      if(self.cost.nil?)
        self.cost = thing.cost
      else
        self.cost += thing.cost
      end
    end
    self.save
    thing
  end

  def update(order_params)
    status = order_params[:status]
    paymaster = User.find(order_params[:paymaster_id])
    if self.status != status
      if(Order.statuses[:close] == Order.statuses[status] && self.open?)
        createSettlements 
        self.collected = 0
      elsif(Order.statuses[:open]  == Order.statuses[status])
        Settlement.where(["order_id = ?", self.id]).each {|settlement| settlement.destroy}
        self.collected = 0
      end
    end
    super(order_params.merge(paymaster: paymaster))
  end

  def delete_thing(thing_id)
    thing = self.things.find(thing_id)
    if(!thing.nil?)
      self.cost -= thing.cost
      self.save
    end
    thing.destroy
  end

  def delete_user(user_id)
    @user = self.orders_users.find(user_id)
    self.orders_users.delete(@user);
    self.save
  end

  def add_user(user)
    self.orders_users.push(user) unless self.include_user?(user)
    self.save
  end

  def include_user?(user)
    self.orders_users.exists?(user.id)
  end

  def is_owner?(user)
    !self.owner.nil? && self.owner.id == user.id
  end

  def is_paymaster?(user)
    !self.paymaster.nil? && self.paymaster.id == user.id
  end

  def can_delete?(user)
    self.is_owner?(user)
  end

  def can_edit?(user)
    self.is_owner?(user)
  end

  def can_settling?(user)
    self.is_owner?(user) || self.is_paymaster?(user)
  end

  def paymaster_candidate
    self.orders_users + [self.owner]
  end

  def recalc_collected
    self.collected = 0
    self.settlements.each { |settlement| self.collected += settlement.paid}
    self.save
  end

  private 

  def createSettlements
    grouper = Hash.new(0)
    self.things.each { |thing| grouper[thing.user] += thing.cost}
    grouper.each do |user, to_pay|
      self.settlements.push Settlement.create_for_order(self, user, to_pay)
    end
  end
end
