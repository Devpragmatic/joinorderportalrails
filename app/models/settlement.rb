class Settlement < ApplicationRecord
  belongs_to :order
  belongs_to :user

  def self.create_for_order(order, user, to_pay)
    settlement = Settlement.new
    settlement.order = order
    settlement.user = user
    settlement.to_pay = to_pay
    settlement.paid = 0
    settlement.save
    settlement
  end
end
