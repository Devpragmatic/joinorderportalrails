# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
thingsTable = null
ordersTable = null
settlementsTable = null
$(document).on "turbolinks:load", ->
    thingsTable = $('#thingsTable').DataTable(  {"destroy": true , "retrieve": true}) if $('#thingsTable').length && !$.fn.DataTable.isDataTable( '#thingsTable' )
    ordersTable = $('#ordersTable').DataTable(  {"destroy": true , "retrieve": true}) if $('#ordersTable').length && !$.fn.DataTable.isDataTable( '#ordersTable' )
    settlementsTable = $('#settlementsTable').DataTable(  {"destroy": true , "retrieve": true}) if $('#settlementsTable').length && !$.fn.DataTable.isDataTable( '#settlementsTable' )
$(document).on "turbolinks:before-cache", ->
  thingsTable.destroy() if $('#thingsTable_wrapper').length
  ordersTable.destroy() if $('#ordersTable_wrapper').length
  settlementsTable.destroy() if $('#settlementsTable_wrapper').length