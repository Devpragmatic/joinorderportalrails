class OrdersBaseController < ApplicationController
  protected

  def check_read_permission
    get_order
    unless @order.is_owner?(current_user) || @order.include_user?(current_user)
       redirect_to root_path
    end
  end

  def check_edit_permission
    get_order
    unless @order.can_edit?(current_user)
       redirect_to root_path
    end
  end

  def check_settling_permission
    get_order
    unless @order.can_settling?(current_user) && @order.close?
       redirect_to root_path
    end
  end

  private 
  def get_order
    id = params[:order_id]
    if(id.nil?)
        id = params[:id]
    end
    @order = Order.find(id)
  end
end
