class ThingsController < OrdersBaseController
  before_action :check_read_permission
  before_action :check_delete_permission, only: [:destroy]
  before_action :check_change_permission, only: [:create, :destroy]
  def create
    @order = Order.find(params[:order_id])
    @thing = @order.create_thing(thing_params.merge(user: current_user))
    render 'orders/show'
  end

  def index
    render 'orders/show'
  end

  def show
    render 'orders/show'
  end

  def destroy
    @order = Order.find(params[:order_id])
    @order.delete_thing(params[:id])
    render 'orders/show'
  end

  private

  def thing_params
    params.require(:thing).permit(:name, :cost)
  end

  def check_delete_permission
    id = params[:id]
    thing = Thing.find(id)
    unless thing.can_delete?(current_user)
       redirect_to root_pathdelete
    end
  end

  def check_change_permission
    order = Order.find(params[:order_id])
    unless order.open?
       redirect_to root_pathdelete
    end
  end
end
