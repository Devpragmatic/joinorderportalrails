class OrdersUsersController < OrdersBaseController
  before_action :check_read_permission
  def create
    @order = Order.find(params[:order_id])
    @user = OrdersUser.find_by email: user_params[:email]
    if(!@user.nil?)
      @order.add_user(@user)
    else
      @order.errors[:base] << t(:user_is_not_exists)
    end
    render 'orders/edit'
  end

  def index
    redirect_to 'orders/edit'
  end

  def show
    redirect_to 'orders/edit'
  end

  def destroy
    @order = Order.find(params[:order_id])
    @order.delete_user(params[:id])
    render 'orders/edit'
  end

  private
  
  def user_params
    params.require(:orders_user).permit(:email)
  end
end
