class OrdersController < OrdersBaseController
  before_action :check_read_permission, only: [:show]
  before_action :check_edit_permission, only: [:edit, :update, :destroy]
  def index
    @orders = Order.left_outer_joins(:orders_users).where("orders.owner_id = ? OR orders_orders_users.orders_user_id = ?", current_user.id, current_user.id)
  end
  
  def new 
    @order = Order.new
  end
  
  def create
    @order = Order.new(order_params)
    @order.status = 'open'
    @order.owner = current_user
    @order.cost = 0.0
    if @order.save
      redirect_to @order
    else
      render 'new'
    end
  end
  
  def show
    @order = Order.find(params[:id])
  end
    
  def edit
    @order = Order.find(params[:id])
  end

  def update
    @order = Order.find(params[:id])
    if @order.update(order_params)
      redirect_to @order
    else
      render 'edit'
    end
  end

  def destroy
    @order = Order.find(params[:id])
    @order.destroy
    redirect_to orders_path
  end

  private
  
  def order_params
    params.require(:order).permit(:name, :status, :paymaster_id)
  end
end
