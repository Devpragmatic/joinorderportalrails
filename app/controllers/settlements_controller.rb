class SettlementsController < OrdersBaseController
  before_action :check_read_permission
  before_action :check_settling_permission, only: [:update]

  def update
    @order = Order.find(params[:order_id])
    @settlement = @order.settlements.find(params[:id])
    @settlement.update(settlement_params)
    @order.recalc_collected
    render 'orders/show'
  end

  def index
    render 'orders/show'
  end

  def show
    render 'orders/show'
  end

  private
  
  def settlement_params
    params.require(:settlement).permit(:paid)
  end
end
