class AddOrderRefToThings < ActiveRecord::Migration[5.0]
  def change
    add_reference :things, :order, foreign_key: true
  end
end
