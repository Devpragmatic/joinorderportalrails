class AddPaymasterRefToOrder < ActiveRecord::Migration[5.0]
  def change
    add_reference :orders, :paymaster, foreign_key: true
  end
end
