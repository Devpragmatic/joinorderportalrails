class AddCollectedToOrders < ActiveRecord::Migration[5.0]
  def change
    add_column :orders, :collected, :float
  end
end
