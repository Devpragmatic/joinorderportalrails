class CreateSettlements < ActiveRecord::Migration[5.0]
  def change
    create_table :settlements do |t|
      t.references :order, foreign_key: true
      t.references :user, foreign_key: true
      t.float :to_pay
      t.float :paid

      t.timestamps
    end
  end
end
