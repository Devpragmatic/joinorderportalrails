class AddUserRefToOrder < ActiveRecord::Migration[5.0]
  def change
    add_reference :orders, :owner, foreign_key: true
  end
end
