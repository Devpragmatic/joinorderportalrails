class CreateOrderUserJoinTable < ActiveRecord::Migration[5.0]
  def change
    create_join_table :orders, :orders_users do |t|
      # t.index [:order_id, :user_id]
      t.index [:orders_user_id, :order_id], unique: true
    end
  end
end
